package oe.barbero.kata.ohce.infrastructure

class StubInputOutput: InputOutputMechanism {
    var output: String = ""

    override fun read(): String {
        return "Stop!"
    }

    override fun write(text: String) {
        output = text
    }

}
