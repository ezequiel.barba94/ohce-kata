package oe.barbero.kata.ohce.infrastructure

import action.Reverse
import oe.barbero.kata.ohce.action.Greet
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class GreetingsTests {
    private val io = StubInputOutput()
    private val commandLine = CommandLine(io)
    private val operator = Operator(commandLine, Greet(), Reverse())

    @Test
    fun `should greet on start`() {
        operator.start("Pedro")

        Assertions.assertThat(io.output).isEqualTo(
            """
            > Buenos días Pedro!
            $ """.trimIndent()
        )
    }
}