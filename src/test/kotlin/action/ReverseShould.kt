package oe.barbero.kata.ohce.action

import action.Reverse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ReverseShould {
    @Test
    fun `reverse word sent`() {
        val reverse = Reverse()

        val reversed = reverse("palabra")

        assertThat(reversed).isEqualTo("arbalap")
    }
}