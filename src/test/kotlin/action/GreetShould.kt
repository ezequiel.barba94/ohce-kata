package oe.barbero.kata.ohce.action

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class GreetShould {
    @Test
    fun `greet with the name given`() {
        val greet = Greet()

        val greetings = greet("Pedro")

        Assertions.assertThat(greetings).isEqualTo("Buenos días Pedro!")
    }

}