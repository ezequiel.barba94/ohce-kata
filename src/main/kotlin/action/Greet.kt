package oe.barbero.kata.ohce.action

class Greet {
    operator fun invoke(name: String) = "Buenos días $name!"
}
