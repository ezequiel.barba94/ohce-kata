package action

class Reverse {
    operator fun invoke(word: String) = word.reversed()
}
