package oe.barbero.kata.ohce.infrastructure

open class CommandLine(private val io: InputOutputMechanism) {
    fun write(text: String) {
        io.write(
            """
            > $text
            $ 
        """.trimIndent()
        )
    }

    fun read() = io.read()
}