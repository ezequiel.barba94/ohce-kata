package oe.barbero.kata.ohce.infrastructure

import action.Reverse
import oe.barbero.kata.ohce.action.Greet

fun main(args: Array<String>) {
    val operator = Operator(CommandLine(SystemIO()), Greet(), Reverse())
    operator.start(args[0])
}
