package oe.barbero.kata.ohce.infrastructure

import action.Reverse
import oe.barbero.kata.ohce.action.Greet

class Operator(private val commandLine: CommandLine, private val greet: Greet, private val reverse: Reverse) {
    fun start(name: String) {
        commandLine.write(greet(name))
        val command = commandLine.read()
        while (isNotExitCode(command))
            commandLine.write(reverse(commandLine.read()))
    }

    private fun isNotExitCode(command: String): Boolean {
        return command !== "Stop!"
    }
}
