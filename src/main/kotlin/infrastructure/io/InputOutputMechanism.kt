package oe.barbero.kata.ohce.infrastructure.io

interface InputOutputMechanism {
    fun read(): String
    fun write(text: String)
}
