package oe.barbero.kata.ohce.infrastructure.io

class SystemIO : InputOutputMechanism {
    override fun read(): String = readLine() ?: ""
    override fun write(text: String) = println(text)
}